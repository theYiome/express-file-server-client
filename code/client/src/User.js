import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';
import { ProfileDialog, LoginForm } from './UserForm.js'

class UserProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = { form: {username: null, password: null}, user: null, loginOpen: false, logoutOpen: false };
    this.handleClick = this.handleClick.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
  }
  getColor() {
    return this.state.user ? 'secondary' : 'inherit';
  }
  componentDidMount() {
    fetch('http://localhost:8080/csrfToken', {credentials: 'include'})
    .then(res => res.json())
    .then(res => {
      const { CSRFToken } = res;
      this.setState( { CSRFToken } );
    });
  }
  handleClick() {
    this.setState(state => {
      if (!state.user) {
        return { loginOpen: true };
      } else {
        return { logoutOpen: true };
      }
    });
  }
  handleInputChange(e) {
    const {name, value} = e.target;
    this.setState(state => {
      state.form[name] = value;
      return state;
    });
  }
  handleSubmit() {
    const { username, password } = this.state.form;
    const { CSRFToken } = this.state;
    fetch('http://localhost:8080/login', {
        method: 'POST',
        credentials: 'include',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'csrf-token': CSRFToken
        },
        body: `username=${username}&password=${password}`
    }).then(res => {
      //TODO load user profile here
      if (res.status === 200) { 
        this.setState({ form: {username: '', password: ''}, user: { username }});
      }
      this.setState({ loginOpen: false});
      this.props.onLogin(username); 
    }).catch(console.log);

  }
  handleCancel() {
    this.setState({ loginOpen: false, form: { username: '', password: ''} });
  }
  handleLogout() {
    fetch('http://localhost:8080/logout', { method: 'GET', credentials: 'include' })
    .then(res => {
      if (res.status === 200) { 
        this.setState({ user: null });
      }
      this.setState({ logoutOpen: false });
      this.props.onLogin(''); 
    });
  }
  render() {
    return (
      <>
        <IconButton
          color="inherit"
          onClick={this.handleClick}
        >
          <AccountCircle color={this.getColor()} />
        </IconButton>
        <LoginForm
          open={this.state.loginOpen}
          handleSubmit={this.handleSubmit}
          handleCancel={this.handleCancel}
          handleInputChange={this.handleInputChange}
          values={this.state.form}
        />
        <ProfileDialog
          open={this.state.logoutOpen}
          handleCancel={() => { this.setState({logoutOpen: false}) }}
          handleLogout={this.handleLogout}
          profile={this.state.user}
        />
      </> 
    );
  }
}

export default UserProfile;

