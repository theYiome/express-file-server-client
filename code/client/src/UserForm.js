import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

function LoginForm(props) {
  const { open, handleSubmit, handleInputChange, handleCancel, values } = props;
  const username = values.username ? values.username : '';
  const password = values.password ? values.password : '';
  const disabled = ( username === '' || password === '' ) ? true : false;
  return (
    <Dialog open={open} aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title">Log In</DialogTitle>
      <DialogContent>
        <div>
          <TextField 
            autoFocus 
            id="username-field" 
            required 
            label="User Name"
            name="username"
            value={username}
            onChange={handleInputChange}
          />
        </div>
        <div>
          <TextField 
            id="password-field" 
            label="Password" 
            type="password"
            name="password"
            value={password}
            onChange={handleInputChange}
          />
        </div>
      </DialogContent>
      <DialogActions>
        <Button type="submit" onClick={handleSubmit} disabled={disabled} color="primary">
          Login
        </Button>
        <Button onClick={handleCancel} color="primary">
          Cancel
        </Button>
      </DialogActions>
    </Dialog>
  );
}

function ProfileDialog(props) {
  const { open, profile, handleCancel, handleLogout } = props;
  return (
    <Dialog open={open} onClose={handleCancel} >
      <DialogTitle id="logout-dialog-title">
        {"User"}
      </DialogTitle>
      <DialogContent>
        <div>
        <DialogContentText id='logout-text'>
          User {profile ? profile.username : ''} logged in. Do you want to log out?
        </DialogContentText>
        </div>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleLogout}> Log Out </Button>
        <Button onClick={handleCancel}> Cancel </Button>
      </DialogActions>
    </Dialog>
  );
}

export { ProfileDialog, LoginForm };
