const fs = require('fs').promises;
const chai = require('chai');

chai.use(require('chai-string'));
expect = chai.expect;
chai.use(require('chai-like'));
chai.use(require('chai-things'));

describe('Vulnerabilities', function () {
    let page
    const username = 'czarek';
    const password = 'pass';

    async function json() {
        await page.content();
        return await page.evaluate(() =>  {
            return JSON.parse(document.querySelector("body").innerText);
        });
    }

    async function getToken() {
      const res = await page.goto('http://localhost:8080/csrfToken');
      const data = await json();
      return data.CSRFToken;
    } 

    async function login({ origin = ORIGIN, csrfToken = true } = {}) {
      page = await browser.newPage();
      let token = null
      if (csrfToken) {
        token = await getToken();
      }
      await page.setRequestInterception(true);
      page.once('request', req => {
        req.continue({
          method: 'POST',
          postData: `username=${username}&password=${password}`,
          headers: {
              ...req.headers(),
              'Origin': origin,
              'Content-Type': 'application/x-www-form-urlencoded',
              'csrf-token': token
          }
        });
      });
      return await page.goto(LOGIN_URL);
    } 

    async function logout() {
      return await page.goto(LOGOUT_URL);
    } 

    before(async function () {
        const res = await login();
        expect(res.ok()).to.be.true;
    });

    after(async function () {
        page.close();
    });

    it('should not use wildcard in Cross Origin policy', async function () {
        const resp = await page.goto(`${TEST_URL}/czarek/`);
        const headers = resp.headers();
        const policy = headers['access-control-allow-origin'];
        if (policy) {
            expect(policy).not.to.equal("*");
        }
    });

    it('should not allow injection - wrong content type', async function () {
        const injectFile = "../../drive/<img src onerror='document.body.append(`NOT `+`GOOD`)'>";
        await fs.writeFile(injectFile, '');
        await page.goto(`${TEST_URL}/czarek/`);
        let bodyText = await page.evaluate(() => document.body.innerText);
        await fs.unlink(injectFile)
        expect(bodyText).to.not.include("NOT GOOD");
    });

    it('should set correct content type', async function () {
        const resp = await page.goto(`${TEST_URL}/czarek/`);
        const headers = resp.headers();
        expect(headers['content-type']).to.startsWith('application/json');
    });

    it('should set correct content security policy - script src (prevent .js from /drive)', async function () {
        const resp = await page.goto(`${TEST_URL}/czarek/`);
        const headers = resp.headers();
        expect(headers['content-security-policy']).to.include('script-src http://localhost:8080/js');
    });
    
    it('should not allow path enumeration - basic check', async function () {
        const res = await page.goto(`${TEST_URL}../`);
        expect(res.ok()).to.be.false;
    });
    
    it('should not allow path enumeration - OS root dir', async function () {
        const res = await page.goto(`${TEST_URL}/`);
        expect(res.ok()).to.be.false;
    });

    it('should not allow path enumeration - forward and back', async function () {
        const res = await page.goto(`${TEST_URL}/czarek/../..`);
        expect(res.ok()).to.be.false;
    });

    it('should not allow path enumeration - similar path - startsWith', async function () {
        let drive2 = process.env['ROOT_DIR'] + '2';
        await fs.mkdir(`${ROOT_DIR}/czarek2`);
        const res = await page.goto(`${TEST_URL}/czarek2/`);
        await fs.rmdir(`${ROOT_DIR}/czarek2`);
        expect(res.ok()).to.be.false;
    });
    
    it('should not allow path enumeration - correct path but information leak', async function () {
        const res = await page.goto(`${TEST_URL}../drive/czarek/`);
        expect(res.ok()).to.be.false;
    });


    it('should contain nosniff header', async function() {
        const resp = await page.goto(`${TEST_URL}/czarek/`);
        const headers = resp.headers();
        expect(headers['x-content-type-options']).to.include('nosniff');
    });

    it('should contain headers protecting from clickjacking', async function() {
        const resp = await page.goto(`${TEST_URL}/czarek/`);
        const headers = resp.headers();
        expect(headers['content-security-policy']).to.include("frame-ancestors 'none'");
    });

    it('session cookie should have httpOnly attribute set', async function() {
        await page.goto(`${TEST_URL}/czarek/`);
        const cookies = await page.cookies();
        expect(cookies).to.contain.something.like({ name: 'connect.sid', httpOnly: true });
    });
    
    it('should not allow cross origin requests', async function () {
        await logout();
        const res = await login({origin: 'http://localhost:3000', csrfToken: false});
        expect(res.ok()).to.be.false;
    });
});
