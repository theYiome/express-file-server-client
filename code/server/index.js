const express = require('express');
const path = require('path');
const swaggerUI = require('swagger-ui-express');
const YAML = require('yamljs');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const csrf = require('csurf');
const csrfProtection = csrf({ cookie: true });
const { ROOT_DIR, PORT, DRIVE_ENDPOINT, DOCS_ENDPOINT } = process.env;
const passport = require('passport');  // authentication
const LocalStrategy = require('passport-local').Strategy;

const app = express();

app.use((req, res, next) => {
    res.set('Access-Control-Allow-Origin', 'http://localhost:3000');
    res.set('Access-Control-Allow-Credentials', 'true');
    res.set('Access-Control-Allow-Headers', req.get("Access-Control-Request-Headers"));
    res.set('Content-Security-Policy', "frame-ancestors 'none'; script-src http://localhost:8080/js;");
    next();
});

const { find, findById } = require('./src/users');

passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    const user = findById(id);
    done(err, user);
});

passport.use(new LocalStrategy(
    function (username, password, done) {

        try {
            const user = find(username);

            if (!user)
                return done(null, false);

            if (user.password !== password)
                return done(null, false);

            return done(null, user);
        }
        catch (err) {
            return done(err);
        }
    }
));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(passport.initialize());
app.use(passport.session());

// added middelware
app.use(cookieParser());
app.use(csrfProtection);

const RootDir = ROOT_DIR ? ROOT_DIR : process.cwd();

const { dirRenderer, HtmlRenderer, JsonRenderer } = require('./src/dir-renderer');
const renderer = dirRenderer(RootDir, new JsonRenderer);


const DriveEndpoint = DRIVE_ENDPOINT ? `/${DRIVE_ENDPOINT}` : '/drive';

app.use(DriveEndpoint, renderer);


// const swaggerDocument = YAML.load('./api/network-drive-1.0.0-resolved.yaml');
// const DocsEndpoint = DOCS_ENDPOINT ? `/${DOCS_ENDPOINT}` : '/api-docs';
// app.use(DocsEndpoint, swaggerUI.serve, swaggerUI.setup(swaggerDocument));

app.post('/login',
    passport.authenticate('local'),
    function (req, res) {
        res.status(200).send();
    }
);

app.get('/logout',
    function (req, res) {
        req.logout();
        res.status(200).send();
    }
);

app.get('/csrfToken',
    function (req, res) {
        res.status(200).json({ CSRFToken: req.csrfToken() });
    }
);

const Port = PORT ? PORT : 8080;
server = app.listen(Port);

module.exports = server;

