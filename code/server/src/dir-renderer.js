const fs = require('fs').promises;
const path = require('path');

function HtmlRenderer() {

  function makeDirListEntry(prefix, entry) {
    const link = path.join(prefix, entry);
    return `<li><a href=${link}>${entry}</a></li>\n`;
  }

  this.dirListing = function(dir, url) {
    return dir.map(entry => makeDirListEntry(entry.dir, entry.name)).join('');
  }

  this.contentType = "text/html";
  this.contentDisposition = "inline";
}

function JsonRenderer() {
  this.contentType = "application/json";
  this.contentDisposition = "attachment";
}

async function handleDirectory(res, full_path, after_drive) {
  // console.log("handleDirectory:", full_path);
  try {
      const filenames = await fs.readdir(full_path);

      const isDir = async (p) => {
          try {
              await fs.readFile(p);
              return "F";
          } catch(err) {
              return "D";
          }
      }

      const response_object = await Promise.all(filenames.map(async (x) => {
          r = await isDir(path.join(full_path, x));
          return {
              name: x,
              dir: after_drive,
              type: r
          }
      }));

      // console.log(response_object);
      
      res.status(200).send(JSON.stringify(response_object, null, 4));
  }
  catch (error) {
      // console.log(error);
      const path_elements = full_path.split(path.sep);
      const filename = path_elements[path_elements.length - 1];
      res.status(400).send(`Unable to open file ${filename}`);
  }
}

const dirRenderer = (RootDir, renderer) => {
  
  return (req, res, next) => {

      res.setHeader('Content-Type', 'application/json');
      if (req.user) {
        const path_arr = req.originalUrl.split("/");
        const doubleDotsRemoved = path_arr.filter((val, ind, arr) => val !== "..");
        console.log(doubleDotsRemoved);

        const after_drive = path.join("/", ...doubleDotsRemoved.slice(2));
        const relative_path = path.join(after_drive);
        const full_path = path.join(RootDir, relative_path);
  
        
        if (path_arr.length < 3 || path_arr[1] !== "drive" || path_arr[2] !== req.user.username) {
            res.status(400).send(`Unable to open file ${req.originalUrl}`);
        } else {
    
            fs.readFile(full_path).then(fileContent => {
    
                // console.log(fileContent.toString());
                res.status(200).send(fileContent.toString());
    
            }).catch(error => {
                handleDirectory(res, full_path, after_drive);
            });
        }
      } else {
          res.status(401).send();
      }

  }

}

module.exports = { dirRenderer, HtmlRenderer, JsonRenderer };
