import React from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import FileList from './FileList.js';
import UserProfile from './User.js';

const SERVER_URL='http://localhost:8080';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  title: {
    flexGrow: 1,
  },
}));

async function fetchRemoteFolder(url) {
  return fetch(`${SERVER_URL}/drive${url}`, {mode: 'cors', credentials: 'include' })
    .then( res => res.json())
    .then( items => ({url, items}))
    .catch(e => {
      return ({url, items:[]});
    });
}

function App(props) {
  const [files, setFiles] = React.useState({url:'', items:[]});
  const classes = useStyles();

  const fetchFolder = (url) => {
    fetchRemoteFolder(url).then(lst => setFiles(lst))
  }

  const location = useLocation();
  const history = useHistory();
  React.useEffect( () => {fetchFolder(location.pathname) }, [location]);

  return (
    <Container maxWidth="sm" className={classes.root}>
      <Box my={4}>
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6" className={classes.title}>Net Storage</Typography>
            <div>
              <UserProfile onLogin={(user) => history.push(`/${user}`)}/>
            </div>
          </Toolbar>
        </AppBar>
        <FileList files={files} serverUrl={SERVER_URL}/>
      </Box>
    </Container>
  );
}

export default App;
