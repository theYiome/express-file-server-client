import {Link as RLink} from 'react-router-dom'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';
import FolderIcon from '@material-ui/icons/Folder';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';

function pathReduce(url) {
  return url
    .split('/')
    .filter( f => f.length > 0)
    .reduce( ({arr, path}, name) => {
      path = `${path}/${name}`;
      return {arr:[...arr, {path, name}], path};
    }, {arr:[{name:'Home', path:''}], path:''})
    .arr;
}

function FileIcon(props) {
  return (
    <ListItemIcon>
      {props.f.type === 'D' ? (<FolderIcon />) :  (<InsertDriveFileIcon />)}
    </ListItemIcon>
  );
}

function Crumbs(props) {
  const {url} = props;
  const paths = pathReduce(url);
  const current = paths.pop() || {};

  return (
    <Breadcrumbs>
      {paths.map( p => (
        <Link key={p.path} color="inherit" component={RLink} to={p.path}>{p.name}</Link>
      ))}
      <Typography color="textPrimary">{current.name}</Typography>
    </Breadcrumbs>
  );
}

function FileList(props) {
  const {files, serverUrl} = props;

  const items = [
    ...files.items.filter(f => f.type === 'D').sort(),
    ...files.items.filter(f => f.type === 'F').sort()
  ];

  const decorateLink = (f) => {
    let url = `${f.dir==='/'?'':f.dir}/${f.name}`;
    return f.type === 'D' ?
      {component:RLink, to:url} :
      {component:'a', href:`${serverUrl}/drive/${url}`, download:true /*, target:'_blank'*/};
  }

  return (
    <>
     <List dense={true} component="nav" subheader={
         <Box component="div" id="nested-list-subheader" p={2}>
            <Crumbs url={files.url} />
         </Box>
       }
     >
       {items.map( (f) => (
          <ListItem button key={f.name} {...decorateLink(f)} >
            <FileIcon f={f} />
            <ListItemText primary={f.name} />
          </ListItem>
        ))}
      </List>
    </>
  );
}

export default FileList;

