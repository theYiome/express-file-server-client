const db = {
  'czarek': {
    id: '1',
    name: 'Cezary',
    surname: 'Manista',
    password: 'pass'
  },
  'czarek2': {
    id: '1',
    name: 'Cezary Klon',
    surname: 'Manista',
    password: 'pass'
  },

  'amos': {
    id: '2',
    name: 'Amos',
    surname: 'Burton',
    password: 'imthatman'
  }
};

function find(username) {
  return db[username];
}

function findById(id) {
  const keys = Object.keys(db);
  const username = keys.find(user => db[user].id == id);
  return { ...db[username], username };
}

module.exports = { find, findById }
