const { expect } = require('chai');
const puppeteer = require('puppeteer');

global.ORIGIN = 'http://localhost:3000'
global.TEST_URL = 'http://localhost:8080/drive';
global.LOGIN_URL = 'http://localhost:8080/login';
global.LOGOUT_URL = 'http://localhost:8080/logout';
global.ROOT_DIR = 'drive';

const opts = {
    //headless: false,
    //slowMo: 100,
    //timeout: 1000
};

before (async function () {
    process.env['ROOT_DIR'] = ROOT_DIR;
    browser = await puppeteer.launch(opts);
    server = require('../index');
});

after (function () {
    browser.close();
    server.close();
});
